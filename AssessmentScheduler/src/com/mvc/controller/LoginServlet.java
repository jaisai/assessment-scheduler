package com.mvc.controller;

import java.io.IOException;
import java.util.Hashtable;

import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NameParser;
import javax.naming.NamingEnumeration;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.LoginBean;
import com.mvc.dao.LoginDao;

public class LoginServlet extends HttpServlet {
	public static boolean isLogin = false;
	public LoginServlet() {
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Here username and password are the names which I have given in the
		// input box in Login.jsp page. Here I am retrieving the values entered
		// by the user and keeping in instance variables for further use.
		
		//code for logout - Killing the session
		String killSession = request.getParameter("killSession");
		
		//Fetching the user name
		String username = request.getParameter("username");
		if (killSession != null) {
			HttpSession session=request.getSession();  
            session.invalidate(); 
            isLogin=false;
            response.sendRedirect(request.getContextPath()+"/Login.jsp");
            //response.setHeader("Cache-control", "private,no-store,no-cache");
			//request.setAttribute("username",null);
            
			//request.getRequestDispatcher("Login.jsp").forward(request, response);
		} else {
			response.setContentType("text/html;charset=UTF-8");
			response.setContentType("text/html;charset=UTF-8");

			//Fetching the password
			String password = request.getParameter("password");
			RequestDispatcher rd;
			
//code for connecting to ldap server for checking the Student id and password 
			String url = "ldap://proxis.nwmissouri.edu:389";
			String[] attrIdsToSearch = new String[] { "memberOf", "givenName",
					"SN" };
			String SEARCH_BY_SAM_ACCOUNT_NAME = "(sAMAccountName=%s)";
			String userBase = "DC=nwmissouri,DC=edu";

			Hashtable<String, String> environment = new Hashtable<>();

			environment.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			environment.put(Context.PROVIDER_URL, url);
			environment.put(Context.SECURITY_AUTHENTICATION, "simple");
			environment.put(Context.SECURITY_PRINCIPAL, username
					+ "@nwmissouri.edu");
			environment.put(Context.SECURITY_CREDENTIALS, password);

			try {
				InitialDirContext context = new InitialDirContext(environment);

				String filter = String.format(SEARCH_BY_SAM_ACCOUNT_NAME,
						username);
				SearchControls constraints = new SearchControls();
				constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
				constraints.setReturningAttributes(attrIdsToSearch);
				NamingEnumeration results = context.search(userBase, filter,
						constraints);

				// Fail if no entries found
				if (results == null || !results.hasMore()) {
					return;
				}
				// Get result for the first entry found
				SearchResult result = (SearchResult) results.next();
				// parse through string and retrieve first and last name
				String resultStr = result.toString();
				String firstName = resultStr.substring(
						resultStr.indexOf("Name: ") + 6,
						resultStr.indexOf(", m"));
				String lastName = resultStr.substring(
						resultStr.indexOf("sn=sn: ") + 7,
						resultStr.indexOf("}"));
				String fullName = firstName + " " + lastName;
				HttpSession namesession = request.getSession();
				namesession.setAttribute("namesession", fullName);

				request.setAttribute("fullName", fullName);
				// SearchResult result = (SearchResult) results.next();
				NameParser parser = context.getNameParser("");
				Name entryName = parser.parse(new CompositeName(result
						.getName()).get(0));

				//Setting the user name and passowrd to Login bean
				LoginBean loginBean = new LoginBean();
				loginBean.setUserName(username);
				loginBean.setPassword(password);
				HttpSession session = request.getSession();
				session.setAttribute("username", username);
				
				LoginDao loginDao = new LoginDao();
				int role = LoginDao.authenticateUser(loginBean);
				HttpSession rolesession = request.getSession();
				rolesession.setAttribute("rolesession", role);
				
				int count = LoginDao.userexists(username);
				if (count == 0)
					LoginDao.insertnewUser(username, firstName, lastName);
				isLogin=true;
				if(session !=null){
				request.getRequestDispatcher("HomeServlet").forward(request,
						response);
				}
				
				

			}

			catch (Exception e) {
				request.setAttribute("incorrect", "Incorrect credentials");
				rd = request.getRequestDispatcher("Login.jsp");
				rd.forward(request, response);
			}
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

}
