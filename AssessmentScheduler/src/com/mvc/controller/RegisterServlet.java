package com.mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.SessionBean;
import com.mvc.dao.SessionDao;

public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RegisterServlet() {
		super();

	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session1 = request.getSession();
		if(session1 !=null && LoginServlet.isLogin){
		//Fetching values on click of register button in Home.jsp
		String value = request.getParameter("values");
		String[] valuesFromHomepage = value.split("/");
		String sessionID = valuesFromHomepage[0];
		String assessmentID = valuesFromHomepage[1];
		String buttonType = valuesFromHomepage[2];
		//Fetching Userid from seeion which is set in login servlet
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("username");

		List<SessionBean> registeredSessionsList = new ArrayList<>();
		List<SessionBean> upcomingSessionsList = new ArrayList<>();
		
		//On click of register button in Home page
		if (buttonType.equals("registerbutton")) {
			// Getting the count of sessions registered for that assessment 
			int count = SessionDao.getAlreadyRegisteredCount(userId,assessmentID);
			//if count is zero then register
			if (count ==0 )
			{
				//calling method in session dao which will register a user to that session
				registeredSessionsList = SessionDao.registerForSession(sessionID, assessmentID, userId);
				upcomingSessionsList = SessionDao.getUpcomingSessionsList();
				//Method to send calendar event to the user 
				SessionDao.sendMail(registeredSessionsList.get(registeredSessionsList.size()-1),userId);
				
			}
			else//if the user is already registered to any session in that assessment
			{
				registeredSessionsList = SessionDao.loadRegisteredSessions(userId);
				upcomingSessionsList = SessionDao.getUpcomingSessionsList();
				//Displaying message
				request.setAttribute("Incorrect", "You are already registered for a session in this assessment.  If you want to change the session, "
						+ "please unregister this session and then re-register for a different session");
				request.setAttribute("errorMessage", "errorMessage");
			}
		}
		// onclick of unregister button
		else {
			//calling method in session dao which will unregister a user from tha particular session
			registeredSessionsList = SessionDao.unregisterForSession(sessionID,
					assessmentID, userId);
			upcomingSessionsList = SessionDao.getUpcomingSessionsList();
			
		}
		request.setAttribute("registeredSessionsList", registeredSessionsList);
		request.setAttribute("upcomingSessionsList", upcomingSessionsList);
		request.getRequestDispatcher("/Home.jsp").forward(request, response);
		}
		else{
			request.getRequestDispatcher("/Login.jsp").forward(request, response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			processRequest(request, response);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			processRequest(request, response);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

}
