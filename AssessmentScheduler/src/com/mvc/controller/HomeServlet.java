package com.mvc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.SessionBean;
import com.mvc.dao.SessionDao;


public class HomeServlet extends HttpServlet {

	
	private static final long serialVersionUID = -8695390562771634939L;

	public HomeServlet() {
		super();
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			ClassNotFoundException {

		HttpSession session = request.getSession();
		if(session !=null && LoginServlet.isLogin){
		String userId = (String) session.getAttribute("username");

		List<SessionBean> registeredSessionsList = new ArrayList<>();
		List<SessionBean> upcomingSessionsList = new ArrayList<>();

		//Fetching the list of registered sessions based on user id
		registeredSessionsList = SessionDao.loadRegisteredSessions(userId);
		request.setAttribute("registeredSessionsList", registeredSessionsList);

		//Fetching the list of all upcoming sessions
		upcomingSessionsList = SessionDao.getUpcomingSessionsList();
		request.setAttribute("upcomingSessionsList", upcomingSessionsList);

		//Redirecting to Home.jsp
		request.getRequestDispatcher("/Home.jsp").forward(request, response);
		}
		else{
			request.getRequestDispatcher("/Login.jsp").forward(request, response);
		}

	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}
	}
}
