package com.mvc.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mvc.bean.AssessmentBean;
import com.mvc.bean.SessionBean;
import com.mvc.bean.UserBean;
import com.mvc.dao.AssessmentDao;
import com.mvc.dao.SessionDao;

public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ClassNotFoundException,
			ServletException, IOException, ParseException {
		HttpSession session = request.getSession();
		if(session !=null && LoginServlet.isLogin){
		List<UserBean> UBList = new ArrayList<>();
		List<SessionBean> sessionsListForAssessment = new ArrayList<>();
		try {

			// code for creating a session
			
			//Fetching the entered session values from createsession.jsp page
			String assessmentID = request.getParameter("assessmentID");
			String startDate = request.getParameter("startDate");
			String startTime = request.getParameter("startTime");
			String endDate = request.getParameter("endDate");
			String endTime = request.getParameter("endTime");
			String building = request.getParameter("building");
			String numOfParticipants = request.getParameter("numOfParticipants");
			java.sql.Timestamp timestampStartTime = null, timestampEndTime = null;

			if (startDate != null && endDate != null) {

				try {
					
					SimpleDateFormat TSformatter = new SimpleDateFormat(
							"MM/dd/yyyy hh:mm a");
					
					// merging the string values of date,time and then
					// converting them into timestamp -StartTime
					
					String mergedStartDateTime = startDate + " " + startTime;
					
					java.util.Date parsedStartDate = TSformatter
							.parse(mergedStartDateTime);
					timestampStartTime = new java.sql.Timestamp(
							parsedStartDate.getTime());
					

					// converting the string values to date and time seperately
					// and then converting them into timestamp -endTime
					String mergedEndDateTime = endDate + " " + endTime;
					java.util.Date parsedEndDate = TSformatter
							.parse(mergedEndDateTime);
					timestampEndTime = new java.sql.Timestamp(
							parsedEndDate.getTime());

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			List<SessionBean> sessionsList = new ArrayList<>();
			List<AssessmentBean> Assesslist = new ArrayList<>();
			
			// creating session
			if (assessmentID != null && startTime != null && startDate != null
					&& endTime != null && endDate != null
					&& numOfParticipants != null) {

				SessionBean sessionbean = new SessionBean();
				// Setting the values to session bean
				sessionbean.setAssessmentID(Integer.parseInt(assessmentID));
				sessionbean.setSessionStart(timestampStartTime);
				sessionbean.setSessionEnd(timestampEndTime);
				sessionbean.setDateCreated(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				sessionbean.setDateModified(new java.sql.Timestamp(
						new java.util.Date().getTime()));
				sessionbean.setNumOfParticipants(Integer
						.parseInt(numOfParticipants));
				//Creating a session in database with session bean values
				SessionDao.createSession(sessionbean);
				//Fetching the list of sessions for that assessment
				sessionsList = SessionDao.getAllSessionsForAnAssessment(assessmentID);
				//Fetching the assessment details - assessment name, description ....
				Assesslist = AssessmentDao.getAssessmentDetails(assessmentID);
				request.setAttribute("assessmentDeatils", Assesslist);
				request.setAttribute("sessionsListForAssessment", sessionsList);
				// After creating a session redirecting to SessionsForAssessment.jsp
				RequestDispatcher requestDispatcher;
				requestDispatcher = request
						.getRequestDispatcher("SessionsForAssessment.jsp");
				requestDispatcher.forward(request, response);

			}
			
			else {
				//Fetching the assessmentid on click of sessions button in admin.jsp page
				String assessmentIDFromAdminJsp = request.getParameter("sessionButton");
				//Fetching the assessmentid on click of delete button in admin.jsp page
				String deleteAssessment = (String) request.getParameter("values");
				//Fetching sessionId from sessionsforassessment.jsp page on click of registrations button
				String sessionIDFromSessionsForAssessJSP = (String) request.getParameter("registrationsForSession");
				//Fetching session id and assessmentid on click of delete button in sessionsforassessment.jsp page
				String deleteIDs = (String) request.getParameter("deleteSession");
				//Fetching assessmentId on click of delete all sessions button in sessionsforassessment.jsp page
				String deleteAllSessionsInAssessment = (String) request.getParameter("deleteAllSessions");

				// getting the list of sessions for an assessment on click of sessions button in admin page
				if (assessmentIDFromAdminJsp != null) {
					sessionsListForAssessment = SessionDao.getAllSessionsForAnAssessment(assessmentIDFromAdminJsp);
					Assesslist = AssessmentDao.getAssessmentDetails(assessmentIDFromAdminJsp);
					request.setAttribute("assessmentDeatils", Assesslist);
					request.setAttribute("sessionsListForAssessment", sessionsListForAssessment);
					RequestDispatcher requestDispatcher;
					requestDispatcher = request
							.getRequestDispatcher("SessionsForAssessment.jsp");
					requestDispatcher.forward(request, response);
				}
				// deleting assessment on click of delete button in admin.jsp page
				else if (deleteAssessment != null) {
					List<AssessmentBean> assessmentsList = new ArrayList<>();
					sessionsListForAssessment = SessionDao.getAllSessionsForAnAssessment(deleteAssessment);
					if(sessionsListForAssessment.isEmpty())
					SessionDao.deleteAssessment(deleteAssessment);
					else{
						request.setAttribute("CannotDelete", "In order to delete an assessment, please delete all of its sessions.");
						
					}
					assessmentsList = AssessmentDao.getAllAssessments();
					String SearchValue="";
					request.setAttribute("SearchValue",SearchValue);
					request.setAttribute("allAssessmentsList", assessmentsList);
					request.getRequestDispatcher("/Admin.jsp").forward(request,
							response);

				}

				// getting the list of registered users for a session
				if (sessionIDFromSessionsForAssessJSP != null) {

					List<SessionBean> SBList = new ArrayList<>();
					int id = Integer
							.parseInt(sessionIDFromSessionsForAssessJSP);
					SBList = SessionDao.getSessionDetails(id);
					UBList = SessionDao.getRegistrationsForSession(id);
					request.setAttribute("sessionDetails", SBList);
					request.setAttribute("registeredUsersList", UBList);
					request.getRequestDispatcher("RegisteredUsers.jsp")
							.forward(request, response);
				} 
				//deleting particular session
				else if (deleteIDs != null) {
					String[] ids = deleteIDs.split("/");
					String sessionID = ids[0];
					String assessId = ids[1];
					List<SessionBean> SBList = new ArrayList<>();
					SessionDao.deleteSession(sessionID);
					SBList = SessionDao.getAllSessionsForAnAssessment(assessId);
					Assesslist = AssessmentDao.getAssessmentDetails(assessId);
					request.setAttribute("assessmentDeatils", Assesslist);
					request.setAttribute("sessionsListForAssessment", SBList);
					RequestDispatcher requestDispatcher;
					requestDispatcher = request
							.getRequestDispatcher("SessionsForAssessment.jsp");
					requestDispatcher.forward(request, response);
				} 
				//deleting all sessions
				else if (deleteAllSessionsInAssessment != null) {
					List<SessionBean> SBList = new ArrayList<>();
					SessionDao
							.deleteAllSessionsInAssessment(deleteAllSessionsInAssessment);
					SBList = SessionDao
							.getAllSessionsForAnAssessment(deleteAllSessionsInAssessment);
					Assesslist = AssessmentDao
							.getAssessmentDetails(deleteAllSessionsInAssessment);
					request.setAttribute("assessmentDeatils", Assesslist);
					request.setAttribute("sessionsListForAssessment", SBList);
					RequestDispatcher requestDispatcher;
					requestDispatcher = request
							.getRequestDispatcher("SessionsForAssessment.jsp");
					requestDispatcher.forward(request, response);
				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		}
		else{
			request
			.getRequestDispatcher("/Login.jsp").forward(request, response);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
