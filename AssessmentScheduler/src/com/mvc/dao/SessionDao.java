package com.mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.mvc.bean.SessionBean;
import com.mvc.bean.UserBean;
import com.mvc.util.DBConnection;

public class SessionDao {

	public static void createSession(SessionBean sessionbean)
			throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement statement = null;
		try {

			con = DBConnection.createConnection();
			String query = "insert into sessions(assessmentId,sessionStart,sessionEnd,dateCreated,dateModified,numOfParticipants,numOfParticipantsEnrolled,buildingCode,roomNumber) values(?,?,?,?,?,?,?,?,?)";
			statement = con.prepareStatement(query);

			statement.setInt(1, sessionbean.getAssessmentID());
			java.sql.Timestamp dateString = new java.sql.Timestamp(
					new java.util.Date().getTime());
			statement.setTimestamp(2, sessionbean.getSessionStart());
			statement.setTimestamp(3, sessionbean.getSessionEnd());
			statement.setTimestamp(4, sessionbean.getDateCreated());
			statement.setTimestamp(5, sessionbean.getDateModified());

			statement.setInt(6, sessionbean.getNumOfParticipants());
			statement.setInt(7, 0);
			statement.setString(8, "GS");
			statement.setString(9, "1950");

			statement.executeUpdate();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void deleteSession(String sessionID)
			throws ClassNotFoundException {

		deleteEnrollmentsinSession(sessionID);
		Connection con = null;
		Statement statement = null;
		int resultSet = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "delete from sessions where sessionID=" + sessionID;
			resultSet = statement.executeUpdate(query);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void deleteEnrollmentsinSession(String sessionID)
			throws ClassNotFoundException {

		Connection con = null;
		Statement statement = null;
		int resultSet = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "delete from enrollments where sessionID="
					+ sessionID;
			resultSet = statement.executeUpdate(query);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void deleteAllSessionsInAssessment(String assessmentID)
			throws ClassNotFoundException {
		try {
			List<SessionBean> list = getAllSessionsForAnAssessment(assessmentID);
			for (SessionBean sb : list) {
				String sessionId = String.valueOf(sb.getSessionID());
				deleteSession(sessionId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void deleteAssessment(String assessmentID)
			throws ClassNotFoundException {

		deleteAllSessionsInAssessment(assessmentID);
		Connection con = null;
		Statement statement = null;
		int resultSet = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "delete from assessments where assessmentID="
					+ assessmentID;
			resultSet = statement.executeUpdate(query);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static List<SessionBean> registerForSession(String sessionId,
			String assessID, String userId) throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement statement = null;
		List<SessionBean> registeredList = null;
		String query="";
		try {

			con = DBConnection.createConnection();
			java.sql.Timestamp dateString = new java.sql.Timestamp(
					new java.util.Date().getTime());
			if(sessionId!=null &&assessID!=null&& userId!=null && dateString.toString()!=null)
			{
			 query = "insert into enrollments(sessionID,assessmentID,dateRegistered,userID) Values(?,?,?,?)";
			statement = con.prepareStatement(query);
			statement.setInt(1, Integer.parseInt(sessionId));
			statement.setInt(2, Integer.parseInt(assessID));
			statement.setTimestamp(3, dateString);
			statement.setString(4, userId);
			statement.executeUpdate();
			}
			registerUpdateforEnrollment(Integer.parseInt(sessionId),Integer.parseInt(assessID));
			registeredList = loadRegisteredSessions(userId);
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return registeredList;

	}

	public static List<SessionBean> getUpcomingSessionsList()
			throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<SessionBean> upComingSessionslist = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessments.assessmentName,sessions.sessionStart,sessions.sessionEnd,assessments.assessmentDesc,sessions.sessionID,sessions.assessmentID,"
						+ "sessions.numOfParticipants,sessions.numOfParticipantsEnrolled,sessions.buildingCode,sessions.roomNumber "
						+ "from assessments,sessions where assessments.assessmentID=sessions.assessmentID and sessions.sessionStart>sysdate() order by 1,2";

			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				String assessmentName = resultSet
						.getString("assessments.assessmentName");
				Timestamp sessionStart = resultSet
						.getTimestamp("sessions.sessionStart");
				Timestamp sessionEnd = resultSet
						.getTimestamp("sessions.sessionEnd");
				String assessmentDesc = resultSet
						.getString("assessments.assessmentDesc");
				int sessionId = resultSet.getInt("sessions.sessionID");
				int assessmentId = resultSet.getInt("sessions.assessmentID");
				int numofParticipants = resultSet
						.getInt("sessions.numOfParticipants");
				int numOfParticipantsEnrolled = resultSet
						.getInt("sessions.numOfParticipantsEnrolled");
				String buildingCode = resultSet
						.getString("sessions.buildingCode");
				String roomNumber = resultSet.getString("sessions.roomNumber");
				SessionBean sessionObject = new SessionBean(sessionId,
						sessionStart, sessionEnd, null, null,
						numofParticipants, numOfParticipantsEnrolled,
						buildingCode, roomNumber, assessmentId, assessmentName,
						assessmentDesc, null, null);
				upComingSessionslist.add(sessionObject);
				sessionObject = null;
				// System.out.println(upComingSessionslist);
				
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return upComingSessionslist;
	}

	public static int getAlreadyRegisteredCount(String userId,
			String assessmentID) throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		int count = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select count(*) as enrolled from enrollments where userID='"
					+ userId + "' and assessmentID=" + assessmentID;

			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				count = resultSet.getInt(1);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;

	}

	public static void unregisterUpdateforEnrollment(int sessionid,int assessid)
			throws ClassNotFoundException {

		Connection con = null;
		Statement statement = null;
		int resultSet = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "update sessions set numOfParticipantsEnrolled=numOfParticipantsEnrolled-1 where sessionId="+ sessionid +" and assessmentID="+assessid;
			resultSet = statement.executeUpdate(query);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void registerUpdateforEnrollment(int sessionId,int assessid)
			throws ClassNotFoundException {

		Connection con = null;
		Statement statement = null;
		int resultSet = 0;
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "update sessions set numOfParticipantsEnrolled=numOfParticipantsEnrolled+1 where  sessionId="+ sessionId +" and assessmentID="+assessid;
			resultSet = statement.executeUpdate(query);
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static List<SessionBean> unregisterForSession(String id,
			String assessID, String userId) throws ClassNotFoundException {
		Connection con = null;
		PreparedStatement statement = null;
		List<SessionBean> registeredList = null;
		try {

			con = DBConnection.createConnection();

			int sessionID = Integer.parseInt(id);

			int assessmentID = Integer.parseInt(assessID);

			String user = userId;
			String query = "delete from enrollments where sessionID="
					+ sessionID + " and assessmentId=" + assessmentID
					+ " and userID='" + user + "'";
			statement = con.prepareStatement(query);
			statement.executeUpdate();
			unregisterUpdateforEnrollment(sessionID,assessmentID);
			registeredList = loadRegisteredSessions(user);
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return registeredList;

	}

	public static List<SessionBean> loadRegisteredSessions(String user)
			throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<SessionBean> registerdSessions = new ArrayList<>();
		String newUser = "'" + user + "'";
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessments.assessmentName,sessions.sessionStart,sessions.sessionEnd,assessments.assessmentDesc,sessions.sessionID,sessions.assessmentID,sessions.numOfParticipants,sessions.numOfParticipantsEnrolled,sessions.buildingCode,sessions.roomNumber from assessments,sessions, enrollments where assessments.assessmentID=sessions.assessmentID  and sessions.sessionID=enrollments.sessionID and enrollments.userID="
					+ newUser+" order by enrollments.dateRegistered";

			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {

				String assessmentName = resultSet
						.getString("assessments.assessmentName");
				Timestamp sessionStart = resultSet
						.getTimestamp("sessions.sessionStart");
				Timestamp sessionEnd = resultSet
						.getTimestamp("sessions.sessionEnd");
				String assessmentDesc = resultSet
						.getString("assessments.assessmentDesc");
				int sessionId = resultSet.getInt("sessions.sessionID");
				int assessmentId = resultSet.getInt("sessions.assessmentID");
				int numofParticipants = resultSet
						.getInt("sessions.numOfParticipants");
				int numOfParticipantsEnrolled = resultSet
						.getInt("sessions.numOfParticipantsEnrolled");
				String buildingCode = resultSet
						.getString("sessions.buildingCode");
				String roomNumber = resultSet.getString("sessions.roomNumber");
				SessionBean sessObject = new SessionBean(sessionId,
						sessionStart, sessionEnd, null, null,
						numofParticipants, numOfParticipantsEnrolled,
						buildingCode, roomNumber, assessmentId, assessmentName,
						assessmentDesc, null, null);
				registerdSessions.add(sessObject);
				sessObject = null;
				//System.out.println("resgistered sessions in session dao: "+registerdSessions);
				
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return registerdSessions;

	}

	public static List<SessionBean> getSessionDetails(int id)
			throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<SessionBean> registerdSessions = new ArrayList<>();

		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			String query = "select assessments.assessmentName,sessions.sessionStart,assessments.assessmentDesc,sessions.sessionID,sessions.assessmentID,building.buildingCode  from assessments,sessions,building "
					+ "where assessments.assessmentID=sessions.assessmentID  and sessions.sessionID="
					+ id;

			resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				SessionBean SBean = new SessionBean();

				SBean.setAssessmentName(resultSet.getString("assessmentName"));
				SBean.setSessionStart(resultSet
						.getTimestamp("sessions.sessionStart"));
				SBean.setAssessmentDesc(resultSet.getString("assessmentDesc"));
				SBean.setSessionID(resultSet.getInt("sessions.sessionID"));
				SBean.setAssessmentID(resultSet.getInt("sessions.assessmentID"));
				SBean.setBuildingCode(resultSet.getString("building.buildingCode"));
				registerdSessions.add(SBean);
				
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return registerdSessions;

	}

	public static List<Object> getSessionDetails(String sessionid)
			throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<Object> List = new ArrayList<>();
		try {
			con = DBConnection.createConnection();
			statement = con.createStatement();
			resultSet = statement
					.executeQuery("select assessments.assessmentName, assessments.assessmentDesc, sessions.sessionStart, sessions.sessionEnd, "
							+ "sessions.dateCreated, sessions.numofParticipants from assessments, sessions where assessments.assessmentID =sessions.assessmentID  and sessions.sessionID="
							+ sessionid);

			//System.out.println("session details result set in session dao: "+resultSet);
			while (resultSet.next()) {
				//System.out.println("Details:" + resultSet);
				List.add(resultSet.getString("assessments.assessmentName"));
				List.add(resultSet.getString("assessments.assessmentDesc"));
				List.add(resultSet.getDate("sessions.sessionStart"));
				List.add(resultSet.getDate("sessions.sessionEnd"));
				List.add(resultSet.getDate("sessions.dateCreated"));
				List.add(resultSet.getInt("sessions.numofParticipants"));
			}
			//System.out.println("Result set after while loop:" + resultSet);
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("List from Details DAO:" + List);
		return List;
	}

	public static List<SessionBean> getAllSessionsForAnAssessment(
			String assessmentID) throws ClassNotFoundException {
		Connection con = null;
		// PreparedStatement statement = null;
		ResultSet resultSet = null;
		/*
		 * CallableStatement callableStatement; String count = "";
		 */

		List<SessionBean> sessionsList = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			PreparedStatement statement = con.prepareStatement("");

			statement = con
					.prepareStatement("select assessments.assessmentID,assessments.assessmentName,assessments.assessmentDesc,sessions.sessionStart,sessions.sessionEnd,sessions.sessionID,sessions.numOfParticipantsEnrolled from assessments,sessions "
							+ "where assessments.assessmentID=sessions.assessmentID and sessions.assessmentID=?" +" order by sessions.sessionStart desc");
			int id = Integer.parseInt(assessmentID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				SessionBean sessionbean = new SessionBean();
				sessionbean.setAssessmentID((resultSet.getInt("assessmentID")));
				sessionbean.setAssessmentName((resultSet
						.getString("assessmentName")));
				sessionbean.setAssessmentDesc((resultSet
						.getString("assessmentDesc")));

				sessionbean.setSessionStart((resultSet
						.getTimestamp("sessionStart")));
				sessionbean
						.setSessionEnd((resultSet.getTimestamp("sessionEnd")));
				sessionbean.setSessionID((resultSet.getInt("sessionID")));
				sessionbean.setNumOfParticipantsEnrolled((resultSet
						.getInt("numOfParticipantsEnrolled")));
				sessionsList.add(sessionbean);
			}
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sessionsList;
	}

	public static List<UserBean> getRegistrationsForSession(int id)
			throws ClassNotFoundException {
		Connection con = null;
		ResultSet resultSet = null;

		List<UserBean> UBList = new ArrayList<>();
		try {
			con = DBConnection.createConnection();
			PreparedStatement statement = con.prepareStatement("");
			statement = con
					.prepareStatement("select users.firstName,users.lastName,enrollments.dateRegistered,sessions.sessionStart,assessments.assessmentID, assessments.assessmentName,building.buildingCode,sessions.roomNumber  "
							+ "from assessments.sessions,assessments.users,assessments.assessments,assessments.enrollments,assessments.building "
							+ "where sessions.sessionID=enrollments.sessionID "
							+ "and assessments.assessmentid = sessions.assessmentID "
							+ "and enrollments.userID =users.userID "
							+ "and sessions.sessionID=?;");
			statement.setInt(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				UserBean UBean = new UserBean();
				UBean.setFirstName(resultSet.getString("users.firstName"));
				UBean.setLastName(resultSet.getString("users.lastName"));
				UBean.setRegistrationDate(resultSet
						.getTimestamp("enrollments.dateRegistered"));
				UBean.setSessionStart(resultSet
						.getTimestamp("sessions.sessionStart"));
				UBean.setAssessmentID(resultSet
						.getInt("assessments.assessmentID"));

				UBean.setAssessmentName(resultSet
						.getString("assessments.assessmentName"));
				UBean.setBuildingCode(resultSet.getString("building.buildingCode"));
				UBean.setRoomNumber(resultSet.getString("sessions.roomNumber"));
				UBList.add(UBean);
			}
			con.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return UBList;
	}

	public static void sendMail(SessionBean sessionbean,String userId) throws Exception {
		
		String from = "citedev@nwmissouri.edu";
		String to = userId + "@nwmissouri.edu";
		String host = "smtp.office365.com";
		String port = "587";
		Properties properties = System.getProperties();

		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", port);
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.auth", "true");

		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("citedev@nwmissouri.edu",
						"06C13d17");
			}
		});
		try {
			MimeMessage message = new MimeMessage(session);
			
			message.setSubject("You are registered for assessment: "+sessionbean.getAssessmentName());
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			
			
			String messageValue="YOU ARE REGISTERED FOR ASSESSMENT: "+ sessionbean.getAssessmentName() +
					 "\n     THIS SESSION WILL START AT:"
					 +sessionbean.getSessionStart() + "\n     THIS SESSION WILL END AT: " +sessionbean.getSessionEnd() + "\n     BUILDING: "
					 +sessionbean.getBuildingCode()+ "\n     ROOM NUMBER: " +sessionbean.getRoomNumber();

			 //converting timestamp to utc format
			 SimpleDateFormat dateFormat = new SimpleDateFormat(sessionbean.getSessionStart().toString());
				String convertedTimestamp  = dateFormat.format(new Date());
				convertedTimestamp = convertedTimestamp.substring(0,convertedTimestamp.length()-2);
				//System.out.println(convertedTimestamp);
				
				String [] timeArray = convertedTimestamp.split(" ");
				//System.out.println(timeArray[0]+ "////"+timeArray[1]);
				
				String[] DateArray = timeArray[0].split("-");
				String[] TSArray = timeArray[1].split(":");
				int timeincrement=Integer.parseInt(TSArray[0])+5;
				if(timeincrement<24)
				TSArray[0]=Integer.toString(timeincrement);
				String concatentdDate = "";
				for(String s: DateArray){
					concatentdDate = concatentdDate+s;
				}
				concatentdDate=concatentdDate+"T";
				for(String s: TSArray){
					concatentdDate = concatentdDate+s;
				}
				concatentdDate=concatentdDate+"Z";
				
				//System.out.println(concatentdDate);
			 
			StringBuffer sb = new StringBuffer();
			//System.out.println("session satrt date in session dao: "+sessionbean.getSessionStart());
			StringBuffer buffer = sb
					.append("BEGIN:VCALENDAR\n"
							+ "VERSION:2.0\n"
							+ "PRODID:-//My oncall calendar application//test.com\n"
							+ "METHOD:REQUEST\n"
							+ "BEGIN:VEVENT\n"
							+ "SUMMARY:test\n"
							+ "DTSTART;VALUE=DATE-TIME:"+concatentdDate+"\n"
							+ "DTEND;VALUE=DATE-TIME:"+concatentdDate+"\n"
							+ "UID:0.669475599056\n" + "SEQUENCE:1\n"
							+ "ATTENDEE:"+to+"\n"
							+ "DESCRIPTION:"+messageValue+"\n" + "LOCATION:"+sessionbean.getBuildingCode()+"-"+sessionbean.getRoomNumber()+"\n"
							+ "ORGANIZER:"+from+"\n"
							+ "PRIORITY:5\n" + "STATUS:confirmed\n"
							+ "BEGIN:VALARM\n" + "ACTION:DISPLAY\n"
							+ "DESCRIPTION:Reminder\n"
							+ "TRIGGER;RELATED=START:-PT1H\n" + "END:VALARM\n"
							+ "END:VEVENT\n" + "END:VCALENDAR");

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();
			// Fill the message
			messageBodyPart.setHeader("Content-Class",
					"urn:content-  classes:calendarmessage");
			messageBodyPart.setHeader("Content-ID", "calendar_message");
			messageBodyPart
					.setDataHandler(new DataHandler(new ByteArrayDataSource(
							buffer.toString(), "text/calendar")));// very
																	// important

			// Create a Multipart
			Multipart multipart = new MimeMultipart();
			// Add part one
			multipart.addBodyPart(messageBodyPart);

			// Put parts in message
			message.setContent(multipart);

			// send message
			Transport.send(message);
		} catch (MessagingException me) {
			me.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
