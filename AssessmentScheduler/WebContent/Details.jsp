<%@page import="com.mvc.bean.SessionBean"%>
<%@page import="java.sql.Timestamp"%>
<%@page language="java" import="java.util.*"%>
<%@ page import="com.mvc.dao.DetailsDao"%>
<%@ page import="com.mvc.bean.LoginBean"%>
<%@ page import="java.sql.Date"%>
<%@ page import="com.mvc.dao.AssessmentDao"%>
<html>
   <head>
      <title>Home Page</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="assessment.css">
      <script src="MyJavaScript.js"></script>
      <style type="text/css">
         #roomTextBox {
         width: 35%
         }
      </style>
      <script>
         history.forward();
      </script>
   </head>
   <body>
      <%@include file="Header.html"%>
      <br>
      <br>
      <br>
      <br>
      <%List<SessionBean> dlist = new ArrayList();
         dlist = (List<SessionBean>)request.getAttribute("sessionList");	
         List<Object> clist = new ArrayList<Object>();	
         clist = (List)request.getAttribute("checklist");
         
         
         	//list = DetailsDao.getsession("sessionid");
         	dlist = null;
         	dlist = (List) request.getAttribute("sessionList");
         	Iterator<Object> ditr;
         	String title, desc, build, building=" ", tr=" ", fa=" ";
         	Timestamp start, end;
         	int ssid = 0, aaid = 0, room; 
         	
         		for (SessionBean sb: dlist) {
         					
         		title = sb.getAssessmentName();						
         		desc = sb.getAssessmentDesc();				
         		
         		start = sb.getSessionStart();
         		String startsplit = start.toString();
         		String[] startarray = startsplit.split(" "); 
         		
         		end = sb.getSessionEnd();
         		String endsplit = end.toString();
         		String[] endarray = endsplit.split(" ");
         						
         	
         		build = sb.getBuildingCode();		
         		room = Integer.parseInt(sb.getRoomNumber());				
         		ssid = sb.getSessionID();
         		aaid = sb.getAssessmentID();
         		
         		tr = (String)clist.get(0);
         		fa = (String)clist.get(1);
         		//tr = (String)ditr.next();
         	/* 	tr = 
         		fa = (String)ditr.next();  */
         		%>
      <%
         String et = endarray[1].toString().trim();
         et = et.substring(0, et.length()-2);
         String st = startarray[1].toString().trim();
         st = st.substring(0, st.length() -2);
         %>
      <div class="container" id="MainContainer">
      <div class="row">
         <div id="LeftContentContainer" class="col-md-2 hidden-xs">
            <div class="bg-main"></div>
         </div>
         <div id="MainContentContainer" class="col-md-8">
            <div class="bg-main">
               <div class="container col-md-12 col-md-offset-0">
                  <div class="panel panel-primary" id="createMeetingForm">
                     <div class="panel-heading">
                        <strong id="MainContent_workshopNameLabel" class="h1"
                           name="title"><%=title%> </strong> <br /> <strong class="h3">
                        <span></span>
                        </strong>
                     </div>
                     <div class="panel-body grid">
                        <div class="form-group col-md-12">
                           <div class="form-group">
                              <label for="description">Description:</label>
                              <p>
                                 <span id="MainContent_Description" name="description">
                                 <%=desc%></span>
                              </p>
                           </div>
                        </div>
                        <div class="row-fluid">
                           <div class="form-group col-md-3">
                              <label for="startdatepicker"> Start Date:</label>
                              <div id="startdatepicker" class="input-group">
                                 <input name="ctl00$MainContent$startDate" type="text"
                                    name="startdate" value="<%=startarray[0]%>" id="startDate"
                                    class="form-control" placeholder="Start Date" disabled /> <span
                                    class="input-group-addon"> <span
                                    class="glyphicon glyphicon-calendar"></span>
                                 </span>
                              </div>
                           </div>
                           <div class="row-fluid">
                              <div class="form-group col-md-3">
                                 <label for="startdatepicker"> Start Time:</label>
                                 <div id="startdatepicker" class="input-group">
                                    <input name="ctl00$MainContent$startDate" type="text"
                                       name="starttime" value="<%=st%>" id="startDate"
                                       class="form-control" placeholder="Start Time" disabled />
                                    <span class="input-group-addon"> <span
                                       class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="enddatepicker">End Date:</label>
                                 <div id="enddatepicker" class="input-group date">
                                    <input name="ctl00$MainContent$endDate" type="text"
                                       name="enddate" value="<%=endarray[0]%>" id="endDate"
                                       class="form-control" placeholder="End Date" disabled /> <span
                                       class="input-group-addon"> <span
                                       class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="enddatepicker">End Time:</label>
                                 <div id="enddatepicker" class="input-group date">
                                    <input name="ctl00$MainContent$endDate" type="text"
                                       name="endtime" value="<%=et%>" id="endDate"
                                       class="form-control" placeholder="End Time" disabled /> <span
                                       class="input-group-addon"> <span
                                       class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="enddatepicker">Building:</label>
                                 <div id="enddatepicker" class="input-group date">
                                    <input name="ctl00$MainContent$endDate" type="text"
                                       name="building" value="<%=build%>" id="building"
                                       class="form-control" placeholder="Location" disabled />
                                 </div>
                              </div>
                              <div class="form-group col-md-3">
                                 <label for="enddatepicker">Room Number:</label>
                                 <div id="enddatepicker" class="input-group date">
                                    <input name="ctl00$MainContent$endDate" type="text"
                                       name="room" value="<%=room%>" id="room"
                                       class="form-control" placeholder="Room number" disabled />
                                 </div>
                              </div>
                              <div hidden="true" class="form-group col-md-3">
                                 <div class="form-group">
                                    <label for="roomTextBox">SID</label> <input
                                       name="ctl00$MainContent$roomTextBox" type="text"
                                       name="sidtoservlet" value="<%=ssid%>" id="roomTextBox"
                                       class="form-control" disabled
                                       placeholder="Number of poeple" />
                                 </div>
                              </div>
                              <div hidden="true" class="form-group col-md-3">
                                 <div class="form-group">
                                    <label for="roomTextBox">AID</label> <input
                                       name="ctl00$MainContent$roomTextBox" type="text"
                                       name=aidtoservlet value="<%=aaid%>" id="roomTextBox"
                                       class="form-control" disabled
                                       placeholder="Number of poeple" />
                                 </div>
                              </div>
                              <div hidden="true" class="form-group col-md-3">
                                 <div class="form-group">
                                    <label for="True/False"></label> <input
                                       name="ctl00$MainContent$roomTextBox" type="text"
                                       name=true/false value="<%=request.getParameter("tr")%>" id="true/false"
                                       class="form-control" disabled
                                       placeholder="true/false" />
                                 </div>
                              </div>
                              <%
                                 session.setAttribute("sidtoservlet",ssid );
                                                  	session.setAttribute("aidtoservlet", aaid);
                                 %>
                              <%} %>
                              <%
                                 int sessionidtoservlet = (Integer)session.getAttribute("sidtoservlet");
                                                   int assidtoservlet = (Integer)session.getAttribute("aidtoservlet");
                                 %>
                              <div class="row-fluid">
                                 <div class="col-md-12 btn-toolbar">
                                    <br> 
                                    <form action="HomeServlet" method="post">
                                       <button class="btn btn-defualt pull-left">Back</button>
                                    </form>
                                    <form action="RegisterServlet" method="post">
                                       <%
                                          if(tr != null && fa == null){
                                          
                                          %>
                                       <button type="submit" name="values" id="values"
                                          value="<%=sessionidtoservlet+"/"+assidtoservlet+"/"+"registerbutton"%>"
                                          class="btn btn-success pull-right">&nbsp;&nbsp;Register</button>
                                       <%} %>
                                       <%
                                          if(tr == null && fa != null) {
                                          %>
                                       <button type="submit" class="btn btn-danger pull-right" id="unregisterButton">UnRegister</button>
                                       <input type="hidden" name="values" value="<%=ssid+"/"+aaid+"/"+"unregisterbutton"%>" />
                                       <%} %>
                                       <%-- 		
                                          <%
                                          	else {
                                           %>					
                                          
                                          	<button type="submit" name="values" id="values"
                                          		disabled value="<%=sessionidtoservlet+"/"+assidtoservlet+"/"+"registerbutton"%>"
                                          		class="btn btn-success pull-right">&nbsp;&nbsp;Register</button>
                                          <%} %>	 --%>
                                       <input type="hidden" value="<%=sessionidtoservlet%>"
                                          name="seservlet"> <input type="hidden"
                                          value="<%=assidtoservlet%>" name="aservlet"> <input
                                          type="hidden" value="registerbutton" name="registerbutton">
                                       <%--  <%} %> --%>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div id="RightContentContainer" class="col-md-2">
               <div class="bg-main"></div>
            </div>
         </div>
      </div>
      <!--  Javascript files-->
      <script src="/Workshops/js/jquery-2.1.4.min.js"></script>
     
      <script src="/Workshops/js/bootstrap.js"></script>
      <script src="/Workshops/js/script.js"></script>
      <script type="text/javascript">
         function detailstodata(value) {
         	document.getElementById('register').disabled = true;
         register = new XMLHttpRequest();
         var url = "RegisterServlet?values=" + value;
         register.open("POST", url, true);
         register.send(null);
         window.location.reload();
         
         }
      </script>
   </body>