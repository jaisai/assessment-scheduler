<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <title>Login</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script
         src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script
         src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
         <script>
         history.forward();
      </script>
   </head>
   <body onload="window.history.forward(1);">
      <div>
         <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
               <div class="navbar-header">
                  <img class="navbar-brand nav2" src="images/logo.png"/>
               </div>
               <div id="navbar" class="navbar-collapse collapse">
                  <ul class="nav navbar-nav navbar-right">
                     <li></li>
                  </ul>
               </div>
            </div>
         </nav>
      </div>
      <br>
      <br>
      <br>
      <div class="container">
         <div class="row">
            <div class="col-md-4 col-md-offset-4">
               <div class="panel panel-default">
                  <div class="panel-heading">
                     <strong class="h1">Sign In</strong>
                  </div>
                  <div class="panel-body">
                     <form name="form1" action="LoginServlet" method="post"
                        accept-charset="UTF-8" role="form">
                        <fieldset>
                           <div class="form-group">
                              <input class="form-control" placeholder="Northwest username"
                                 name="username" type="text">
                           </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Northwest password"
                                 name="password" type="password" value="">
                           </div>
                           <%=(request.getAttribute("incorrect") == null) ? ""
                              : request.getAttribute("incorrect")%>
                           <br>
                           <input class="btn btn-lg btn-success btn-block" type="submit"
                              value="Sign In">
                        </fieldset>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>