<%@page import="com.mvc.bean.AssessmentBean"%>
<%@page language="java" import="java.sql.Timestamp"%>
<%@page language="java" import="java.util.Date"%>
<%@page language="java" import="java.text.SimpleDateFormat"%>
<%@page language="java" import="com.mvc.bean.SessionBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page language="java" import="java.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assessment.css">
<script src="MyJavaScript.js"></script>
<script>
    history.forward();</script>
    <script>
    function deleteAllSessions(value){
    	$('#myModal').modal('show');
    	$('#btnYes').click(function(){
    		$('#myModal').modal('hide');
    		window.location="SessionServlet?deleteAllSessions=" + value;
    	});
    	}    
</script>
</head>
<body>
	<%@include file="Header.html"%>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="container col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h1>Sessions</h1>
				<%
					List<SessionBean> list = new ArrayList();
						List<AssessmentBean> assesslist = new ArrayList();
						assesslist = (List<AssessmentBean>)request.getAttribute("assessmentDeatils");
						String assessName=null,assessDesc=null;
						int totalCount=0;
						HttpSession assessIdSession = request.getSession();
						for(AssessmentBean sb :assesslist){
							assessIdSession.setAttribute("assessmentId", sb.getAssessmentID());
							assessName = sb.getAssessmentName();
							assessIdSession.setAttribute("assessmentName", assessName);
							assessDesc = sb.getAssessmentDesc();
						}
						
						list = (List<SessionBean>)request.getAttribute("sessionsListForAssessment");
						for(SessionBean sb :list){
							assessIdSession.setAttribute("assessmentId", sb.getAssessmentID());
							assessIdSession.setAttribute("assessmentName", sb.getAssessmentName());
							assessName = sb.getAssessmentName();
							assessDesc = sb.getAssessmentDesc();
							
							}
				%>
				<h3><%=assessName%></h3>
			</div>
<div class="row">
				<br />
<div class="col-md-6" style="margin-left: 1em">
<h4><%=assessDesc%></h4>
</div>
<div style="margin-right: 2em">
<%if(!list.isEmpty()){ %>
					<button onclick="deleteAllSessions(this.value)"
						value="<%=assessIdSession.getAttribute("assessmentId")%>"
						class="btn btn-danger pull-right glyphicon glyphicon-trash"
						style="margin-left: 1em">Delete all Sessions</button> <%}else{ %>
					<button class="btn btn-danger pull-right glyphicon glyphicon-trash"
						style="margin-left: 1em" disabled="true">Delete all
						Sessions</button> <%} %>
</div>

				
				  
</div>
			<div class="panel-body">
				 
				<div class="row"></div>
				<br />
				<div>
					<table class="table table-hover bg-info" cellspacing="0" border="1"
						style="border-style: None; border-collapse: collapse; table-layout: fixed;">
						<tbody>
							<tr>
								<th scope="col" style="color: White; background-color: #449D44;">Session
									Start</th>
								<th scope="col" style="color: White; background-color: #449D44;">Session
									End</th>
								<th scope="col" style="color: White; background-color: #449D44;">No.
									of enrollments</th>
								<th scope="col" style="color: White; background-color: #449D44;">&nbsp;</th>
							</tr>
						
								<%
								SimpleDateFormat TSformatter = new SimpleDateFormat("yyyy-MM-dd");
								
								String todaysDate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
													
									for(SessionBean sb: list){
										String sessionStartDate=sb.getSessionStart().toString().trim().substring(0, sb.getSessionStart().toString().trim().length()-11);										
										if(TSformatter.parse(todaysDate).before(TSformatter.parse(sessionStartDate))){
								%>
									<tr>
								<td><%=sb.getSessionStart().toString().trim().substring(0, sb.getSessionStart().toString().trim().length()-2)%></td>
								<td><%=sb.getSessionEnd().toString().trim().substring(0, sb.getSessionEnd().toString().trim().length()-2)%></td>

								<td align="center"><%=sb.getNumOfParticipantsEnrolled()%></td>
								<td>
									<form action="SessionServlet" method="get">
										<button class="btn btn-success pull-left"
											>Registrations</button>
										<input type="hidden" value="<%=sb.getSessionID()%>"
											name="registrationsForSession"></form>
									<form action="SessionServlet" method="post">
										<button
											class="btn btn-danger pull-left glyphicon glyphicon-trash"
											style="margin-left: 1em" onclick="return confirm('Are you sure you want to delete this session?')" 
											>Delete</button>
											<input type="hidden" value="<%=sb.getSessionID()+"/"+assessIdSession.getAttribute("assessmentId")%>" name="deleteSession">
											</form>
																			</td>
							</tr>
							<%
							totalCount=totalCount+sb.getNumOfParticipantsEnrolled();
								}else{
							%>
								<tr class="alert alert-danger">
								<td><%=sb.getSessionStart().toString().trim().substring(0, sb.getSessionStart().toString().trim().length()-2)%></td>
								<td><%=sb.getSessionEnd().toString().trim().substring(0, sb.getSessionEnd().toString().trim().length()-2)%></td>

								<td align="center"><%=sb.getNumOfParticipantsEnrolled()%></td>
								<td>
									<form action="SessionServlet" method="get">
										<button class="btn btn-success pull-left"
											>Registrations</button>
										<input type="hidden" value="<%=sb.getSessionID()%>"
											name="registrationsForSession"></form>
									
								
									<form action="SessionServlet" method="post">
										<button
											class="btn btn-danger pull-left glyphicon glyphicon-trash"
											style="margin-left: 1em" onclick="return confirm('Are you sure you want to delete this session?')" 
											>Delete</button>
											<input type="hidden" value="<%=sb.getSessionID()+"/"+assessIdSession.getAttribute("assessmentId")%>" name="deleteSession">
											</form>
																			</td>
							</tr>
							<%
								}
										}
							%>
						</tbody>
					</table>

				</div>
				<div>
					<strong>Total number of students enrolled in this
						assessment: </strong><%=totalCount%>
				</div>
				<br>
				<form action="AssessmentServlet" method="post">
					<button class="btn btn-defualt pull-left">Back</button>
				</form>

				<button class="btn btn-success pull-right"
					onclick='window.top.location.href="CreateSession.jsp?assessmentid=" + <%=assessIdSession.getAttribute("assessmentId")%>;'>Create
					a new session</button>



			</div>

		</div>
		<!-- Modal -->
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Confirm Delete</h4>
					</div>
					<div class="modal-body">
						<p>Do you want to delete all Sessions for this Assessment?</p>
					</div>
					<div class="modal-footer">
						<button id="btnYes" class="btn danger">Yes</button>
						<button data-dismiss="modal" aria-hidden="true" id="btnNo"
							class="btn secondary">No</button>
					</div>
				</div>

			</div>
		</div>
</body>
</html>